"""module pays
"""

class Pays():

    pays_dict = {}

    def __init__(self, nom, capitale):
        self.nom = nom
        self.capitale = capitale
        self.pays_dict[nom] = self

    def __eq__(self, autre):
        return self.nom == autre.nom and self.capitale == autre.capitale

    def __repr__(self):
        return 'pays %s' % self.nom

def init_per_litteral_data():
    """initialise les pays via donnees litterales
       les objets construits sont retournés comme tuple
    """

    data = (
        ('allemagne', 'berlin'),
        ('autriche', 'vienne'),
        ('belgique', 'bruxelles'),
        ('bulgarie', 'sofia'),
        ('chypre', 'nicosie'),
        ('croatie', 'zaghreb'),
        ('danemark', 'copenhague'),
        ('espagne', 'madrid'),
        ('estonie', 'tallinn'),
        ('finlande', 'helsinki'),
        ('france', 'paris'),
        ('grèce', 'athènes'),
        ('hongrie', 'budapest'),
        ('irlande', 'dublin'),
        ('italie', 'rome'),
        ('lettonie', 'riga'),
        ('lituanie', 'vilnius'),
        ('luxembourg', 'luxembourg'),
        ('malte', 'la valette'),
        ('pays-bas', 'amsterdam'),
        ('pologne', 'varsovie'),
        ('portugal', 'lisbonne'),
        ('république-tchèque', 'prague'),
        ('roumanie', 'bucarest'),
        ('slovaquie', 'bratislava'),
        ('slovénie', 'ljubljana'),
        ('suède', 'stockholm')
        )

    objets = []
    for datum in data:
        cap = [x.capitalize() for x in datum]
        objet = Pays(cap[0], cap[1])
        objets.append(objet)
    return objets


